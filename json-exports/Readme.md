Files with `_pp.json` suffix were created by using the original json file parsed with the json_pp tool.

- beer.json : A model about beers and their varieties. Modeleded from the image published at https://ar.pinterest.com/pin/394839092302404945/
- coffee.json : Coffee cultivars. Modeleded from the image published at https://ar.pinterest.com/pin/125186064615469141/
